package demo.integration.infrastructure;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.annotation.ServiceActivator;

import demo.integration.pojos.WeatherDataRequest;
import demo.integration.pojos.WeatherDataResponse;
import demo.integration.webservice.config.WebServiceConfiguration;
import demo.integration.webservice.mapping.WebServiceMapping;

@Configuration
@Import({WebServiceConfiguration.class, WebServiceMapping.class})
public class IntegrationInfrastructure {
    
    @ServiceActivator(inputChannel="inputDataChannel")
    public WeatherDataResponse weatherDataForCity(final WeatherDataRequest request) {
    	final WeatherDataResponse response = new WeatherDataResponse();
    	response.setCity(request.getCity());
    	response.setTemperature("34");
    	return response;
    }
}
